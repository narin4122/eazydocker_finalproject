package com.example.tick.eazydocker;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.tick.eazydocker.R;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class OneFragment extends ListFragment implements AdapterView.OnItemClickListener{

    Button Add;
    SQLiteDatabase data;
    Cursor mCursor;
    SQLiteDatabase data2;
    Button Delete;
    int bb ;



    public OneFragment() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        myDB myDb = new myDB(getActivity());
//        myDb.getWritableDatabase(); // First method

        myDB2 myDb2 = new myDB2(getActivity());
//        myDb2.getWritableDatabase(); // First method





    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one, container, false);
        final myDB db = new myDB(getActivity());



        Add = (Button) view.findViewById(R.id.button);

        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View view = inflater.inflate(R.layout.dialog, null);
                builder.setView(view);
                final EditText host = (EditText) view.findViewById(R.id.hostname);
                final EditText username = (EditText) view.findViewById(R.id.ip);
                final EditText password = (EditText) view.findViewById(R.id.port);




                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        long flg1 = db.InsertData(host.getText().toString(),username.getText().toString(),password.getText().toString());
                        if(flg1 > 0)
                       {
                            Toast.makeText(getActivity(),"Insert(1) Data Successfully",
                                    Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getActivity(),"Insert(1) Data Failed.",
                                    Toast.LENGTH_LONG).show();
                        }
                        // Check username password
                        UpdateList();

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.create();

                builder.show();
            }
        });

        Delete = (Button) view.findViewById(R.id.button3);

        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                ClickToDelete(bb);
                Toast.makeText(getActivity(), "Item: " + bb, Toast.LENGTH_SHORT).show();


            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        UpdateList();


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
        final myDB2 db2 = new myDB2(getActivity());
        final myDB db1 = new myDB(getActivity());
        //UpdateList();




        //bb = id;



        //Toast.makeText(getActivity(), "Item: " + position, Toast.LENGTH_SHORT).show();
    }

    public void UpdateList(){
        final myDB2 db2 = new myDB2(getActivity());
        myDB aa = new myDB(getActivity());
//        data = aa.getWritableDatabase();
//        data2 = db2.getWritableDatabase();
        mCursor = data.rawQuery("SELECT " + myDB.ID + ", "  + myDB.NAME + ", "+ myDB.IP
                + ", " + myDB.PORT + " FROM " + myDB.TABLE_MEMBER, null);

        ArrayList<String> dirArray = new ArrayList<String>();
        mCursor.moveToFirst();
        ListView lv = (ListView)getActivity().findViewById(R.id.listView2);

        while ( !mCursor.isAfterLast() ){
            dirArray.add(mCursor.getString(mCursor.getColumnIndex(myDB.NAME)) + "\n"
                    + "IP : " + mCursor.getString(mCursor.getColumnIndex(myDB.IP)) + "\t\t"
                    //+ "ID : " + mCursor.getString(mCursor.getColumnIndex(myDB.ID)) + "\t\t"
                    + "PORT : " + mCursor.getString(mCursor.getColumnIndex(myDB.PORT)));
            mCursor.moveToNext();
        }
        lv.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, dirArray));

        //ArrayAdapter<String> adapterDir = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, dirArray);
        //setListAdapter(adapterDir);
        //getListView().setOnItemClickListener(this);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1
                    , int arg2, long arg3) {
                mCursor.moveToPosition(arg2);
                bb = mCursor.getInt(mCursor.getColumnIndex(myDB.ID));
                int flg1 = db2.InsertData(bb);
                Toast.makeText(getActivity(), "Item: " + flg1, Toast.LENGTH_SHORT).show();

            }
        });


    }

    public int ClickToDelete(final int position){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog, null);
        builder.setView(view);




        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                final myDB db1 = new myDB(getActivity());
                db1.DeleteData(position);
                //Toast.makeText(getActivity(), "Item: " + position, Toast.LENGTH_SHORT).show();
                // Check username password
                UpdateList();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create();

        builder.show();
        return 1;

    }

    public void DeleteList(){


    }








}
