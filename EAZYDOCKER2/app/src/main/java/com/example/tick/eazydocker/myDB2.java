package com.example.tick.eazydocker;

/**
 * Created by tick on 11/3/2016.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

public class myDB2 extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "number";

    // Table Name
    public static final String TABLE_MEMBER = "selected";
    public static final String SELECTED = "SELECTED";




    public myDB2(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }



    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_MEMBER +
                "(MemberID INTEGER PRIMARY KEY AUTOINCREMENT," +
                SELECTED + " INTEGER);");

        Log.d("CREATE TABLE","Create Table Successfully.");

    }
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {}

    //Insert Data
    public int InsertData(int strMemberID) {
        // TODO Auto-generated method stub

        try {
            SQLiteDatabase db;
            db = this.getWritableDatabase(); // Write Data
            //db.execSQL("delete from "+ myDB.TABLE_MEMBER + "where i");


            ContentValues Val = new ContentValues();
            Val.put("SELECTED", strMemberID);


            long rows = db.insert(TABLE_MEMBER, null, Val);

            db.close();
            return 0; // return rows inserted.

        } catch (Exception e) {
            return -1;
        }

    }

}
