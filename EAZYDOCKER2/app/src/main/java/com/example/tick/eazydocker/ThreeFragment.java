package com.example.tick.eazydocker;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.example.tick.eazydocker.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;


public class ThreeFragment extends Fragment {
    public ThreeFragment() {
        // Required empty public constructor
    }
    private View mView;
    public TextView vv;

    JSONParser jParser = new JSONParser();
    private static String url_all_products = "http://128.199.173.55/dome_test.py";
    ArrayList<HashMap<String, String>> productsList;
    //private String TAG = ThreeFragment.class.getSimpleName();

    private ProgressDialog pDialog;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "";
    private static final String TAG_DATE = "Date";
    private static final String TAG_CPU = "Mem_per";
    private static final String TAG_MEM = "Cpu_per";
    private static final String TAG_NETWORK = "Net_i";
    JSONArray products = null;
    int eiei = 50;
    //ListView lv ;
    String array[] = new String[eiei];
    String array2[] = new String[eiei];
    String array3[] = new String[eiei];
    String array4[] = new String[eiei];




    private void initData() {

        int[] index = new int[eiei];
        for(int j =0;j < eiei;j++){
            index[j] = j;
        }


        float number[] = new float[eiei];

        for (int i = 0; i < eiei; i++) {
            number[i] = Float.parseFloat(array3[i]);
        }
        float number2[] = new float[eiei];

        for (int i = 0; i < eiei; i++) {
            number2[i] = Float.parseFloat(array2[i]);
        }

        float number3[] = new float[eiei];

        for (int i = 0; i < eiei; i++) {
            number3[i] = Float.parseFloat(array4[i]);
        }


        //int[] incomeA = {4000, 5500, 2300, 2100, 2500, 2900, 3200, 2400, 1800, 2100, 3500, 5900};
        //int[] incomeB = {3600, 4500, 3200, 3600, 2800, 1800, 2100, 2900, 2200, 2500, 4000, 3500};
        //int[] incomeC = {4300, 4000, 3000, 3200, 2400, 2500, 2600, 3400, 3900, 4500, 5000, 4500};

        XYSeries seriesA = new XYSeries("MEM");
        XYSeries seriesB = new XYSeries("CPU");
        XYSeries seriesC = new XYSeries("NETWORK");

        int length = index.length;
        for (int i = 0; i < length; i++) {
            seriesA.add(index[i], number[i]);
            seriesB.add(index[i], number2[i]);
            //seriesC.add(index[i], number3[i]);
        }

        XYSeriesRenderer rendererA = new XYSeriesRenderer();
        rendererA.setPointStyle(PointStyle.POINT);
        rendererA.setColor(Color.RED);
        rendererA.setLineWidth(1);

        XYSeriesRenderer rendererB = new XYSeriesRenderer();
        rendererB.setPointStyle(PointStyle.POINT);
        rendererB.setColor(Color.BLUE);
        rendererB.setLineWidth(1);

        //XYSeriesRenderer rendererC = new XYSeriesRenderer();
        //rendererC.setPointStyle(PointStyle.DIAMOND);
        //rendererC.setColor(Color.GREEN);
        //rendererC.setLineWidth(1);

        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(seriesA);
        dataset.addSeries(seriesB);
        //dataset.addSeries(seriesC);

        XYMultipleSeriesRenderer multipleSeriesRenderer
                = new XYMultipleSeriesRenderer();

        for (int i = 0; i < length; i++) {
            multipleSeriesRenderer.addXTextLabel(i + 1, array[i]);
        }
        multipleSeriesRenderer.setChartTitle("Resource Use");
        multipleSeriesRenderer.setYTitle("Unit");
        multipleSeriesRenderer.setXTitle("Time");
        multipleSeriesRenderer.setZoomButtonsVisible(true);
        multipleSeriesRenderer.setXLabels(0);
        multipleSeriesRenderer.setBackgroundColor(Color.WHITE);
        multipleSeriesRenderer.setApplyBackgroundColor(true);
        multipleSeriesRenderer.setMarginsColor(Color.WHITE);
        multipleSeriesRenderer.setLabelsColor(Color.BLACK);
        multipleSeriesRenderer.setAxesColor(Color.GRAY);
        multipleSeriesRenderer.setYLabelsColor(0, Color.BLACK);
        multipleSeriesRenderer.setXLabelsColor(Color.BLACK);

        multipleSeriesRenderer.addSeriesRenderer(rendererA);
        multipleSeriesRenderer.addSeriesRenderer(rendererB);
        //multipleSeriesRenderer.addSeriesRenderer(rendererC);

        drawChart(dataset, multipleSeriesRenderer);
    }

    private void drawChart(XYMultipleSeriesDataset dataset,
                           XYMultipleSeriesRenderer renderer) {
        GraphicalView graphView =
                ChartFactory.getLineChartView(getActivity(), dataset, renderer);

        RelativeLayout container =
                (RelativeLayout) mView.findViewById(R.id.graph_container);

        container.addView(graphView);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_three, container, false);
        mView = rootview;
        //lv = (ListView)getActivity().findViewById(R.id.lvv);
        productsList = new ArrayList<HashMap<String, String>>();


        // Loading products in Background Thread
        new LoadAllProducts().execute();

        //initData();

        return rootview;
    }

    class LoadAllProducts extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading products. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All products from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);

            // Check your log cat for JSON reponse
            Log.d("TAG, All Products: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    products = json.getJSONArray(TAG_PRODUCTS);

                    // looping through All Products
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        // Storing each json item in variable
                        String date = c.getString(TAG_DATE);
                        String cpu = c.getString(TAG_CPU);
                        String mem = c.getString(TAG_MEM);
                        String network = c.getString(TAG_NETWORK);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        map.put(TAG_DATE, date);
                        map.put(TAG_CPU, cpu);
                        map.put(TAG_MEM, mem);
                        map.put(TAG_NETWORK, network);

                        // adding HashList to ArrayList
                        productsList.add(map);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            // updating UI from Background Thread

                    /**
                     * Updating parsed JSON data into ListView
                     * */

            for (int i = 0; i < eiei; i++) {
                array[i] = productsList.get(i).get(TAG_DATE);
            }

            for (int i = 0; i < eiei; i++) {
                array2[i] = productsList.get(i).get(TAG_CPU);
            }

            for (int i = 0; i < eiei; i++) {
                array3[i] = productsList.get(i).get(TAG_MEM);
            }

            for (int i = 0; i < eiei; i++) {
                array4[i] = productsList.get(i).get(TAG_NETWORK);
            }

            //vv = (TextView)getActivity().findViewById(R.id.textView5);
            //for(int i = 0;i< 10 ;i ++) {
            //    vv.setText(productsList.get(i).get(TAG_MEM));
            //}
            initData();


;



        }

    }




}
