package com.example.fannies.projectdocker.manager.http;

import android.content.Context;

import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpManager {

    private static HttpManager instance;

    public static HttpManager getInstance() {
        if (instance == null)
            instance = new HttpManager();
        return instance;
    }

    private Context mContext;
    private ApiService serveice;

    private HttpManager() {
        mContext = Contextor.getInstance().getContext();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://128.199.173.55/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        serveice = retrofit.create(ApiService.class);

    }

    public ApiService getServeice() {
        return serveice;
    }
}
