package com.example.fannies.projectdocker.manager;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.fannies.projectdocker.dao.HostItemCollectionDao;
import com.example.fannies.projectdocker.util.InternetCheckUtil;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class UserManager {

    private String loginStatus;
    private String username;
    private String password;
    private String sessionId;
    private UserManagerListener listener;
    private int userIds;

    public int getUserIds() {
        return userIds;
    }

    public void setUserIds(int userIds) {
        this.userIds = userIds;
    }

    private AsyncTask<Void, Void, Boolean> task;

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public interface UserManagerListener {
        void onLogin();
    }

    public void setListener(UserManager.UserManagerListener listener) {
        this.listener = listener;
    }

    private static UserManager instance;

    public static UserManager getInstance() {
        if (instance == null)
            instance = new UserManager();
        return instance;
    }

    private Context mContext;

    private UserManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public void onClickLogin(String username, String password) {


    }
}
