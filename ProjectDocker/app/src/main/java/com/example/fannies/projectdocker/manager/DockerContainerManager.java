package com.example.fannies.projectdocker.manager;

import android.content.Context;

import com.example.fannies.projectdocker.dao.DockerContainerCollectionDao;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class DockerContainerManager {

    private static DockerContainerManager instance;
    private DockerContainerCollectionDao dao;

    public static DockerContainerManager getInstance() {
        if (instance == null)
            instance = new DockerContainerManager();
        return instance;
    }

    private Context mContext;

    private DockerContainerManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public DockerContainerCollectionDao getDao() {
        return dao;
    }

    public void setDao(DockerContainerCollectionDao dao) {
        this.dao = dao;
    }

}
