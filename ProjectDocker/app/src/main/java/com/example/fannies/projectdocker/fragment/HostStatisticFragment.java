package com.example.fannies.projectdocker.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.fannies.projectdocker.R;
import com.example.fannies.projectdocker.dao.DockerContainerEachStatCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerStatisticCollectionDao;
import com.example.fannies.projectdocker.manager.DockerContainerEachStatManager;
import com.example.fannies.projectdocker.manager.DockerContainerManager;
import com.example.fannies.projectdocker.manager.DockerContainerStatisticManager;
import com.example.fannies.projectdocker.manager.HostListManager;
import com.example.fannies.projectdocker.manager.HttpManager;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by nuuneoi on 11/16/2014.
 */
@SuppressWarnings("unused")
public class HostStatisticFragment extends Fragment {

    private LineChart cpuChart;
    private LineChart memChart;
    private LineChart networkChart;
    private BarChart barcpu;
    private BarChart barmem;
    private BarChart barnet;

    public HostStatisticFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static HostStatisticFragment newInstance(int who) {
        HostStatisticFragment fragment = new HostStatisticFragment();
        Bundle args = new Bundle();
        args.putInt("who", who);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_host_statistic, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here

        cpuChart = (LineChart) rootView.findViewById(R.id.g_cpu);
        CPU();
        memChart = (LineChart) rootView.findViewById(R.id.g_ram);
        RAM();
        networkChart = (LineChart) rootView.findViewById(R.id.g_network);
        NETWORK();
        barcpu =(BarChart)rootView.findViewById(R.id.g_cpu_each);
        BarCPU();
        barmem =(BarChart)rootView.findViewById(R.id.g_ram_each);
        BarMEM();
        barnet= (BarChart)rootView.findViewById(R.id.g_network_each);
        BarNET();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    private void CPU() {
        ArrayList<Entry> entries = new ArrayList<>();
        for (int i = 0; i < DockerContainerStatisticManager.getInstance().getDao().getData().size(); i++) {
            //time = time + 0.05f;
            float times = Float.parseFloat(DockerContainerStatisticManager.getInstance().getDao().getData().get(i).getDate());
            //Log.d("key", String.valueOf(times) + " : " + DockerContainerStatisticManager.getInstance().getDao().getData().get(i).getCpuPer());
            entries.add(new Entry(times, DockerContainerStatisticManager.getInstance()
                    .getDao().getData().get(i).getCpuPer()));
        }
        LineDataSet dataset = new LineDataSet(entries, "CPU IN % AND ZONE TIME IN THAILAND");

        LineData data = new LineData(dataset);
        cpuChart.setData(data); // set the data and list of lables into chart
        cpuChart.setDrawGridBackground(false);
        cpuChart.setDescription("");
        cpuChart.getAxisRight().setDrawLabels(false);

        cpuChart.getAxisRight().setDrawLabels(false);
        XAxis xAxis = cpuChart.getXAxis();
        xAxis.setTextSize(12);


        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

//        xAxis.setValueFormatter(new AxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                if(Math.round(value)<ds.length)
//                {
//                    return  ds[Math.round(value)];
//                }
//                else
//                {
//                    return "..";
//                }
//            }
//
//            @Override
//            public int getDecimalDigits() {
//                return 0;
//            }
//        });
//

        //lineChart.setDescription();
        dataset.setDrawValues(false);
        dataset.setDrawFilled(true); // to fill the below area of line in graph
        dataset.setFillColor(Color.CYAN);
        //dataset.setColor(Color.BLACK); // to change the color scheme
        dataset.setDrawCircles(false);


    }

    private void RAM() {
        ArrayList<Entry> entries = new ArrayList<>();
        for (int i = 0; i < DockerContainerStatisticManager.getInstance().getDao().getData().size(); i++) {
            float times = Float.parseFloat(DockerContainerStatisticManager.getInstance().getDao().getData().get(i).getDate());
            //Log.d("key", String.valueOf(times) + " : " + DockerContainerStatisticManager.getInstance().getDao().getData().get(i).getCpuPer());
            entries.add(new Entry(times, DockerContainerStatisticManager.getInstance()
                    .getDao().getData().get(i).getmPer()));
        }
        LineDataSet dataset = new LineDataSet(entries, "RAM IN MB AND ZONE TIME IN THAILAND");

        LineData data = new LineData(dataset);
        memChart.setData(data); // set the data and list of lables into chart
        memChart.setDrawGridBackground(false);
        memChart.setDescription("");
        memChart.getAxisRight().setDrawLabels(false);

        memChart.getAxisRight().setDrawLabels(false);
        XAxis xAxis = memChart.getXAxis();
        xAxis.setTextSize(12);


        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

//        xAxis.setValueFormatter(new AxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                if(Math.round(value)<ds.length)
//                {
//                    return  ds[Math.round(value)];
//                }
//                else
//                {
//                    return "..";
//                }
//            }
//
//            @Override
//            public int getDecimalDigits() {
//                return 0;
//            }
//        });
//

        //lineChart.setDescription();
        dataset.setDrawValues(false);
        dataset.setDrawFilled(true); // to fill the below area of line in graph
        dataset.setFillColor(Color.GREEN);
        //dataset.setColor(Color.BLACK); // to change the color scheme
        dataset.setDrawCircles(false);


    }

    private void NETWORK() {

        ArrayList<Entry> entries = new ArrayList<>();
        for (int i = 0; i < DockerContainerStatisticManager.getInstance().getDao().getData().size(); i++) {
            float times = Float.parseFloat(DockerContainerStatisticManager.getInstance().getDao().getData().get(i).getDate());
            //Log.d("key", String.valueOf(times) + " : " + DockerContainerStatisticManager.getInstance().getDao().getData().get(i).getCpuPer());
            entries.add(new Entry(times, DockerContainerStatisticManager.getInstance()
                    .getDao().getData().get(i).getNetOut()));
        }
        LineDataSet dataset = new LineDataSet(entries, "NETWORK IN MB AND ZONE TIME IN THAILAND");

        LineData data = new LineData(dataset);
        networkChart.setData(data); // set the data and list of lables into chart
        networkChart.setDrawGridBackground(false);
        networkChart.setDescription("");
        networkChart.getAxisRight().setDrawLabels(false);
        XAxis xAxis = networkChart.getXAxis();
        xAxis.setTextSize(12);


        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

//        xAxis.setValueFormatter(new AxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                if(Math.round(value)<ds.length)
//                {
//                    return  ds[Math.round(value)];
//                }
//                else
//                {
//                    return "..";
//                }
//            }
//
//            @Override
//            public int getDecimalDigits() {
//                return 0;
//            }
//        });
//

        //lineChart.setDescription();
        dataset.setDrawValues(false);
        dataset.setDrawFilled(true); // to fill the below area of line in graph
        dataset.setFillColor(Color.YELLOW);
        //dataset.setColor(Color.BLACK); // to change the color scheme
        dataset.setDrawCircles(false);


    }

    private void BarCPU() {
        final ArrayList<BarEntry> entries = new ArrayList<>();
        final ArrayList<Student> listStudent = Student.getSampleStudentData(10);
        Collections.sort(listStudent, new Comparator<Student>() {
            @Override
            public int compare(Student fruit2, Student fruit1)
            {

                return  Float.compare(fruit1.getScore(),fruit2.getScore());
            }
        });


        int index = 0;
        for (Student student : listStudent) {
            entries.add(new BarEntry(index, student.getScore(),student.getName()));
            index++;
        }


        BarDataSet dataset = new BarDataSet(entries, "# expenditure");
        dataset.setValueTextSize(8);
        dataset.setColors(ColorTemplate.VORDIPLOM_COLORS); // set the color

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataset);

        BarData data = new BarData(dataSets); // initialize BarChart
        barcpu.setData(data);
        barcpu.setHorizontalScrollBarEnabled(true);
        barcpu.getXAxis().setDrawLabels(false);
        barcpu.setDescription("");


//      chart.animateY(3000);
//      chart.animateX(5000);
        //barcpu.animateXY(2000,5000, Easing.EasingOption.EaseInBounce, Easing.EasingOption.EaseInElastic);
        barcpu.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                // when select.
                //Toast.makeText(getContext(), " "+e.getX(), Toast.LENGTH_SHORT).show();

                Toast.makeText(getContext(), " "+listStudent.get(Math.round(e.getX())).getName(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected() {

            }
        });
        YAxis RightAxis = barcpu.getAxisRight();
        RightAxis.setEnabled(false);



    }

    private void BarMEM() {
        final ArrayList<BarEntry> entries = new ArrayList<>();
        final ArrayList<Student> listStudent = Student.getmem(10);
        Collections.sort(listStudent, new Comparator<Student>() {
            @Override
            public int compare(Student fruit2, Student fruit1)
            {

                return  Float.compare(fruit1.getScore(),fruit2.getScore());
            }
        });


        int index = 0;
        for (Student student : listStudent) {
            entries.add(new BarEntry(index, student.getScore(),student.getName()));
            index++;
        }

        BarDataSet dataset = new BarDataSet(entries, "# expenditure");
        dataset.setValueTextSize(8);
        dataset.setColors(ColorTemplate.VORDIPLOM_COLORS); // set the color

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataset);

        BarData data = new BarData(dataSets); // initialize BarChart
        barmem.setData(data);
        barmem.setHorizontalScrollBarEnabled(true);
        barmem.getXAxis().setDrawLabels(false);
        barmem.setDescription("");


//      chart.animateY(3000);
//      chart.animateX(5000);
        //barcpu.animateXY(2000,5000, Easing.EasingOption.EaseInBounce, Easing.EasingOption.EaseInElastic);
        barmem.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                // when select.
                //Toast.makeText(getContext(), " "+e.getX(), Toast.LENGTH_SHORT).show();

                Toast.makeText(getContext(), " "+listStudent.get(Math.round(e.getX())).getName(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected() {

            }
        });
        YAxis RightAxis = barmem.getAxisRight();
        RightAxis.setEnabled(false);



    }

    private void BarNET() {
        final ArrayList<BarEntry> entries = new ArrayList<>();
        final ArrayList<Student> listStudent = Student.getnet(10);
        Collections.sort(listStudent, new Comparator<Student>() {
            @Override
            public int compare(Student fruit2, Student fruit1)
            {

                return  Float.compare(fruit1.getScore(),fruit2.getScore());
            }
        });


        int index = 0;
        for (Student student : listStudent) {
            entries.add(new BarEntry(index, student.getScore(),student.getName()));
            index++;
        }

        BarDataSet dataset = new BarDataSet(entries, "# expenditure");
        dataset.setValueTextSize(8);
        dataset.setColors(ColorTemplate.VORDIPLOM_COLORS); // set the color

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataset);

        BarData data = new BarData(dataSets); // initialize BarChart
        barnet.setData(data);
        barnet.setHorizontalScrollBarEnabled(true);
        barnet.getXAxis().setDrawLabels(false);
        barnet.setDescription("");

//      chart.animateY(3000);
//      chart.animateX(5000);
        //barcpu.animateXY(2000,5000, Easing.EasingOption.EaseInBounce, Easing.EasingOption.EaseInElastic);
        barnet.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                // when select.
                //Toast.makeText(getContext(), " "+e.getX(), Toast.LENGTH_SHORT).show();

                Toast.makeText(getContext(), " "+listStudent.get(Math.round(e.getX())).getName(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected() {

            }
        });
        YAxis RightAxis = barnet.getAxisRight();
        RightAxis.setEnabled(false);



    }

}

class Student {
    float score;
    String name;

    public Student(String name,float score) {
        this.score = score;
        this.name = name;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<Student> getSampleStudentData(int size) {

        ArrayList<Student> student = new ArrayList<>();
        for (int i = 0; i < DockerContainerEachStatManager.getInstance().getDao().getData().size(); i++) {
            String times = DockerContainerEachStatManager.getInstance().getDao().getData().get(i).getName();
            //Log.d("key", String.valueOf(times) + " : " + DockerContainerStatisticManager.getInstance().getDao().getData().get(i).getCpuPer());
            student.add(new Student(times, DockerContainerEachStatManager.getInstance()
                    .getDao().getData().get(i).getCpu()));
        }


        return student;
    }
    public static ArrayList<Student> getmem(int size) {

        ArrayList<Student> student = new ArrayList<>();
        for (int i = 0; i < DockerContainerEachStatManager.getInstance().getDao().getData().size(); i++) {
            String times = DockerContainerEachStatManager.getInstance().getDao().getData().get(i).getName();
            //Log.d("key", String.valueOf(times) + " : " + DockerContainerStatisticManager.getInstance().getDao().getData().get(i).getCpuPer());
            student.add(new Student(times, DockerContainerEachStatManager.getInstance()
                    .getDao().getData().get(i).getMem()));
        }


        return student;
    }

    public static ArrayList<Student> getnet(int size) {

        ArrayList<Student> student = new ArrayList<>();
        for (int i = 0; i < DockerContainerEachStatManager.getInstance().getDao().getData().size(); i++) {
            String times = DockerContainerEachStatManager.getInstance().getDao().getData().get(i).getName();
            //Log.d("key", String.valueOf(times) + " : " + DockerContainerStatisticManager.getInstance().getDao().getData().get(i).getCpuPer());
            student.add(new Student(times, DockerContainerEachStatManager.getInstance()
                    .getDao().getData().get(i).getNet_o()));
        }


        return student;
    }
}
