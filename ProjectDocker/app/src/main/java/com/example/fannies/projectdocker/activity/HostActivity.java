package com.example.fannies.projectdocker.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.example.fannies.projectdocker.R;
import com.example.fannies.projectdocker.adapter.ContainerAdapter;
import com.example.fannies.projectdocker.dao.DockerContainerPeakCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerStatisticCollectionDao;
import com.example.fannies.projectdocker.fragment.AboutFragment;
import com.example.fannies.projectdocker.fragment.DashboardFragment;
import com.example.fannies.projectdocker.fragment.HostContainerFragment;
import com.example.fannies.projectdocker.fragment.HostDashboardFragment;
import com.example.fannies.projectdocker.fragment.HostStatisticFragment;
import com.example.fannies.projectdocker.manager.DockerContainerPeakManager;
import com.example.fannies.projectdocker.manager.DockerContainerStatisticManager;
import com.example.fannies.projectdocker.manager.HostListManager;
import com.example.fannies.projectdocker.manager.HttpManager;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HostActivity extends AppCompatActivity {

    private int who;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_host);
        initInstance();
        myCallForDockerContainerAndStatisticDao();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .add(R.id.contentContainer, HostDashboardFragment.newInstance(who))
                    .commit();
        }
    }
    public void myCallForDockerContainerAndStatisticDao(){
        Call<DockerContainerPeakCollectionDao> callForPeak = HttpManager.getInstance().getServeice().getPeak(
                HostListManager.getInstance().getDao().getHost().get(who).getHostUrl().toString()
                , HostListManager.getInstance().getDao().getHost().get(who).getPort().toString());

        callForPeak.enqueue(new Callback<DockerContainerPeakCollectionDao>() {
            @Override
            public void onResponse(Call<DockerContainerPeakCollectionDao> call, Response<DockerContainerPeakCollectionDao> response) {
                DockerContainerPeakCollectionDao dao = response.body();
                DockerContainerPeakManager.getInstance().setDao(dao);
            }

            @Override
            public void onFailure(Call<DockerContainerPeakCollectionDao> call, Throwable t) {
                Toast.makeText(HostActivity.this, getResources().getString(R.string.onFailure), Toast.LENGTH_SHORT).show();
            }
        });

        Call<DockerContainerStatisticCollectionDao> call = HttpManager.getInstance().getServeice().getStatistic(
                HostListManager.getInstance().getDao().getHost().get(who).getHostUrl().toString()
                , HostListManager.getInstance().getDao().getHost().get(who).getPort().toString());
        call.enqueue(new Callback<DockerContainerStatisticCollectionDao>() {
            @Override
            public void onResponse(Call<DockerContainerStatisticCollectionDao> call, Response<DockerContainerStatisticCollectionDao> response) {
                DockerContainerStatisticCollectionDao dao = response.body();
                DockerContainerStatisticManager.getInstance().setDao(dao);
            }

            @Override
            public void onFailure(Call<DockerContainerStatisticCollectionDao> call, Throwable t) {
                Toast.makeText(HostActivity.this, getResources().getString(R.string.onFailure), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_host, menu);
        return true;
    }

    private void initInstance() {
        who = getIntent().getExtras().getInt("who");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.contentContainer);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        toolbar.setTitle(viewPager.getAdapter().getPageTitle(viewPager.getCurrentItem()));
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(HostActivity.this, DashboardActivity.class));
                DockerContainerPeakManager.getInstance().setDao(null);
                finish();
                break;
            case R.id.about:
                getSupportFragmentManager().beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .replace(R.id.contentContainer, AboutFragment.newInstance())
                        .commit();
                break;
            case R.id.logout:
                LayoutInflater inflater;
                inflater = LayoutInflater.from(HostActivity.this);
                final View logOutDialog = inflater.inflate(R.layout.dialog_logout, null);

                AlertDialog dialog = new AlertDialog.Builder(HostActivity.this)
                        .setView(logOutDialog)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(HostActivity.this, MainActivity.class));
                                finish();
                            }
                        })
                        .setNegativeButton("Cancel", null).create();
                dialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(HostDashboardFragment.newInstance(who), "Dashboard");
        adapter.addFragment(HostContainerFragment.newInstance(who), "Containers");
        adapter.addFragment(HostStatisticFragment.newInstance(who), "Statistic");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
