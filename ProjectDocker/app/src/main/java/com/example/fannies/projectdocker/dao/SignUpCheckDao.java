package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fannies on 1/28/2017.
 */

public class SignUpCheckDao {
    @SerializedName("status")   private Boolean status;

    public Boolean getStatus() {
        return status;
    }
}
