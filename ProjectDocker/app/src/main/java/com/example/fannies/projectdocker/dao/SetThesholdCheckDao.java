package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fannies on 2/2/2017.
 */

public class SetThesholdCheckDao {
    @SerializedName("success")  private Boolean check;

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }
}
