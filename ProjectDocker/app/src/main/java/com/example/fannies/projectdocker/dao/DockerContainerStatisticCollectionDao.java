package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Fannies on 1/24/2017.
 */

public class DockerContainerStatisticCollectionDao {
    @SerializedName("Data")     private List<DockerContainerStatisticDao> data;
    @SerializedName("status")   private Boolean status;

    public List<DockerContainerStatisticDao> getData() {
        return data;
    }

    public void setData(List<DockerContainerStatisticDao> data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
