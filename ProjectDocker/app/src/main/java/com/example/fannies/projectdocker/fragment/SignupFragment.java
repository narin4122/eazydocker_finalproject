package com.example.fannies.projectdocker.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fannies.projectdocker.R;
import com.example.fannies.projectdocker.dao.SignUpCheckDao;
import com.example.fannies.projectdocker.manager.HttpManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by nuuneoi on 11/16/2014.
 */
@SuppressWarnings("unused")
public class SignupFragment extends Fragment implements View.OnClickListener {

    private EditText edtUsername;
    private EditText edtPassword;
    private EditText edtRePassword;
    private Button btnSubmit;

    public SignupFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static SignupFragment newInstance() {
        SignupFragment fragment = new SignupFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
        edtUsername = (EditText) rootView.findViewById(R.id.edtUsername);
        edtPassword = (EditText) rootView.findViewById(R.id.edtPassword);
        edtRePassword = (EditText) rootView.findViewById(R.id.edtRePassword);
        btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    @Override
    public void onClick(View v) {
        if (v == btnSubmit) {
            final ProgressDialog progressDialog;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getContext().getResources().getString(R.string.loading));
            progressDialog.show();
            if (!edtUsername.getText().toString().isEmpty() && !edtPassword.getText().toString().isEmpty()) {
                if (!edtPassword.getText().toString().equals(edtRePassword.getText().toString())) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Wrong password!", Toast.LENGTH_SHORT).show();
                } else {
                    Call<SignUpCheckDao> call = HttpManager.getInstance().getServeice().signUp(edtUsername.getText().toString(), edtPassword.getText().toString());
                    call.enqueue(new Callback<SignUpCheckDao>() {
                        @Override
                        public void onResponse(Call<SignUpCheckDao> call, Response<SignUpCheckDao> response) {
                            SignUpCheckDao dao = response.body();
                            if (dao.getStatus()) {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "Sign up success", Toast.LENGTH_SHORT).show();
                                getActivity().finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "This username already existed!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<SignUpCheckDao> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.onFailure), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } else {
                getActivity().finish();
            }
        }
    }
}