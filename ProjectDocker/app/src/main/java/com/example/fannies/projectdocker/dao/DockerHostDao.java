package com.example.fannies.projectdocker.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fannies on 1/20/2017.
 */

public class DockerHostDao {
    @SerializedName("url")      private String ipAddress;
    @SerializedName("port")     private String port;
    @SerializedName("name")     private String hostName;
    @SerializedName("ids")      private int ids;
    @SerializedName("Status")   private Boolean status;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getIds() {
        return ids;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }
}
