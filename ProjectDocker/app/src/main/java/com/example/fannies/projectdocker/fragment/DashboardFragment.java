package com.example.fannies.projectdocker.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.audiofx.LoudnessEnhancer;
import android.os.Bundle;
import android.support.annotation.BoolRes;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.fannies.projectdocker.R;
import com.example.fannies.projectdocker.activity.DashboardActivity;
import com.example.fannies.projectdocker.activity.HostActivity;
import com.example.fannies.projectdocker.adapter.HostListAdapter;
import com.example.fannies.projectdocker.adapter.ProcessAdapter;
import com.example.fannies.projectdocker.dao.AddHostCheckDao;
import com.example.fannies.projectdocker.dao.DockerContainerCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerEachStatCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerPeakCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerPeakDao;
import com.example.fannies.projectdocker.dao.DockerContainerStatisticCollectionDao;
import com.example.fannies.projectdocker.dao.DockerHostDao;
import com.example.fannies.projectdocker.dao.HostItemCollectionDao;
import com.example.fannies.projectdocker.manager.DockerContainerEachStatManager;
import com.example.fannies.projectdocker.manager.DockerContainerManager;
import com.example.fannies.projectdocker.manager.DockerContainerPeakManager;
import com.example.fannies.projectdocker.manager.DockerContainerStatisticManager;
import com.example.fannies.projectdocker.manager.HostListManager;
import com.example.fannies.projectdocker.manager.HttpManager;
import com.google.gson.annotations.SerializedName;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by nuuneoi on 11/16/2014.
 */
@SuppressWarnings("unused")
public class DashboardFragment extends Fragment implements SwipeMenuListView.OnMenuItemClickListener {

//    private String hostUrl;
//    private String hostPort;

    private LayoutInflater inflater;
    private SwipeMenuCreator creator;
    private SwipeMenuListView listView;
    private HostListAdapter listAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton floatingActionButton;

    public DashboardFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    private void reloadData() {
        Call<HostItemCollectionDao> call = HttpManager.getInstance().getServeice().loadUserLogin(HostListManager.getInstance().getDao().getUsername().toString()
                , HostListManager.getInstance().getDao().getPassword().toString());
        call.enqueue(new Callback<HostItemCollectionDao>() {
            @Override
            public void onResponse(Call<HostItemCollectionDao> call, Response<HostItemCollectionDao> response) {
                swipeRefreshLayout.setRefreshing(false);
                HostItemCollectionDao dao = response.body();
                if (response.isSuccessful()) {
                    HostListManager.getInstance().setDao(dao);
                } else {
                    HostListManager.getInstance().setDao(dao);
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<HostItemCollectionDao> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "Connection problem", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
        listView = (SwipeMenuListView) rootView.findViewById(R.id.listView);
        floatingActionButton = (FloatingActionButton) rootView.findViewById(R.id.fab_add_new_host);
        listAdapter = new HostListAdapter();
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(),R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadData();
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                swipeRefreshLayout.setEnabled(firstVisibleItem == 0);
            }
        });

        creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem delete = new SwipeMenuItem(getActivity());
                delete.setWidth(200);
                delete.setBackground(R.drawable.selector_list);
                delete.setTitle("Delete");
                delete.setIcon(android.R.drawable.ic_menu_delete);
                delete.setTitleColor(Color.DKGRAY);
                menu.addMenuItem(delete);
            }
        };

        listView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {
                swipeRefreshLayout.setEnabled(false);
            }

            @Override
            public void onSwipeEnd(int position) {
                swipeRefreshLayout.setEnabled(true);
            }
        });

        listView.setMenuCreator(creator);
        listView.setOnMenuItemClickListener(this);
        listView.setOnItemClickListener(listViewItemClickListener);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        inflater = LayoutInflater.from(getActivity());
                                                        final View addHostDialog = inflater.inflate(R.layout.dialog_addhost, null);
                                                        final EditText edtName = (EditText) addHostDialog.findViewById(R.id.edt_host_name);
                                                        final EditText edtIp = (EditText) addHostDialog.findViewById(R.id.edt_host_ipaddr);
                                                        final EditText edtPort = (EditText) addHostDialog.findViewById(R.id.edt_host_port);

                                                        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                                                                .setView(addHostDialog)
                                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                                                final ProgressDialog progressDialog;
                                                                                progressDialog = new ProgressDialog(getActivity());
                                                                                progressDialog.setTitle(getContext().getResources().getString(R.string.loading));
                                                                                progressDialog.show();                                                                                if (!edtIp.getText().toString().isEmpty() && !edtPort.getText().toString().isEmpty() && !edtName.getText().toString().isEmpty()) {
                                                                                    Call<AddHostCheckDao> call = HttpManager.getInstance().getServeice().addDockerHost(edtIp.getText().toString()
                                                                                            , edtPort.getText().toString()
                                                                                            , edtName.getText().toString()
                                                                                            , HostListManager.getInstance().getDao().getIds());
                                                                                    call.enqueue(new Callback<AddHostCheckDao>() {
                                                                                        @Override
                                                                                        public void onResponse(Call<AddHostCheckDao> call, Response<AddHostCheckDao> response) {
                                                                                            AddHostCheckDao dao = response.body();
                                                                                            if(dao.getStatus()){
                                                                                                reloadData();
                                                                                                progressDialog.dismiss();
                                                                                                Toast.makeText(getActivity(), "Host added successful", Toast.LENGTH_SHORT).show();
                                                                                            }
                                                                                            else{
                                                                                                progressDialog.dismiss();
                                                                                                Toast.makeText(getActivity(), "This host is already existed.", Toast.LENGTH_SHORT).show();
                                                                                            }
                                                                                            return;
                                                                                        }

                                                                                        @Override
                                                                                        public void onFailure(Call<AddHostCheckDao> call, Throwable t) {
                                                                                            progressDialog.dismiss();
                                                                                            Toast.makeText(getActivity(), getContext().getResources().getString(R.string.onFailure)
                                                                                                    , Toast.LENGTH_SHORT)
                                                                                                    .show();
                                                                                            return;
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        }

                                                                )
                                                                .

                                                                        setNegativeButton("Cancel", null)

                                                                .

                                                                        create();

                                                        dialog.show();
                                                    }
                                                }

        );
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    @Override
    public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
        switch (index) {
            case 0:
                Call<Void> call = HttpManager.getInstance().getServeice().deleteDockerHost(
                        HostListManager.getInstance().getDao().getHost().get(position).getHostUrl().toString()
                        , HostListManager.getInstance().getDao().getHost().get(position).getPort().toString()
                        , HostListManager.getInstance().getDao().getIds());
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(getActivity(), "Delete host success", Toast.LENGTH_SHORT).show();
                        reloadData();
                        return;
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(getActivity(), "Connection problem", Toast.LENGTH_SHORT).show();
                        return;
                    }
                });
        }
        // false : close the menu; true : not close the menu
        return false;
    }


    /************* Listener*************
     * *******************************
     ********************************/
    final AdapterView.OnItemClickListener listViewItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {


            Call<DockerContainerPeakCollectionDao> callForPeak = HttpManager.getInstance().getServeice().getPeak(
                    HostListManager.getInstance().getDao().getHost().get(position).getHostUrl().toString()
                    , HostListManager.getInstance().getDao().getHost().get(position).getPort().toString());

            callForPeak.enqueue(new Callback<DockerContainerPeakCollectionDao>() {
                @Override
                public void onResponse(Call<DockerContainerPeakCollectionDao> call, Response<DockerContainerPeakCollectionDao> response) {
                    DockerContainerPeakCollectionDao dao = response.body();
                    DockerContainerPeakManager.getInstance().setDao(dao);
                }

                @Override
                public void onFailure(Call<DockerContainerPeakCollectionDao> call, Throwable t) {
                    Toast.makeText(getActivity(), getContext().getResources().getString(R.string.onFailure),Toast.LENGTH_SHORT).show();
                }
            });

            Call<DockerContainerEachStatCollectionDao> callEach = HttpManager.getInstance().getServeice().getEachStat(
                    HostListManager.getInstance().getDao().getHost().get(position).getHostUrl().toString()
                    , HostListManager.getInstance().getDao().getHost().get(position).getPort().toString());

            callEach.enqueue(new Callback<DockerContainerEachStatCollectionDao>() {
                @Override
                public void onResponse(Call<DockerContainerEachStatCollectionDao> call, Response<DockerContainerEachStatCollectionDao> response) {
                    DockerContainerEachStatCollectionDao dao = response.body();
                    DockerContainerEachStatManager.getInstance().setDao(dao);
                }

                @Override
                public void onFailure(Call<DockerContainerEachStatCollectionDao> call, Throwable t) {
                    Toast.makeText(getActivity(), getContext().getResources().getString(R.string.onFailure),Toast.LENGTH_SHORT).show();
                }
            });


            Call<DockerContainerCollectionDao> call = HttpManager.getInstance().getServeice().listDockerContainer(
                    HostListManager.getInstance().getDao().getHost().get(position).getHostUrl().toString()
                    , HostListManager.getInstance().getDao().getHost().get(position).getPort().toString());
            call.enqueue(new Callback<DockerContainerCollectionDao>() {
                             @Override
                             public void onResponse(Call<DockerContainerCollectionDao> call, Response<DockerContainerCollectionDao> response) {
                                 DockerContainerCollectionDao dao = response.body();
                                 if (dao.getStatus()) {
                                     Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                                     DockerContainerManager.getInstance().setDao(dao);
                                     Intent intent = new Intent(getActivity(),HostActivity.class);
                                     intent.putExtra("who", position);
                                     startActivity(intent);
                                     getActivity().finish();
                                 } else {
                                     Toast.makeText(getContext(), "No Docker Container in this host.", Toast.LENGTH_SHORT).show();
                                 }
                             }

                             @Override
                             public void onFailure(Call<DockerContainerCollectionDao> call, Throwable t) {
                                 Toast.makeText(getContext(), "Connection problem", Toast.LENGTH_SHORT).show();
                             }
                         }
            );
        }
    };

}
