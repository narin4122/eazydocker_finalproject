package com.example.fannies.projectdocker.manager;

import android.content.Context;

import com.example.fannies.projectdocker.dao.HostItemCollectionDao;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class HostListManager {

    private static HostListManager instance;
    private HostItemCollectionDao dao;

    public static HostListManager getInstance() {
        if (instance == null)
            instance = new HostListManager();
        return instance;
    }

    private Context mContext;

    private HostListManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public HostItemCollectionDao getDao() {
        return dao;
    }

    public void setDao(HostItemCollectionDao dao) {
        this.dao = dao;
    }
}
