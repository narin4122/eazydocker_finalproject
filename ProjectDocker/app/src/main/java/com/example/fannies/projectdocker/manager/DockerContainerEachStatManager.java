package com.example.fannies.projectdocker.manager;

import android.content.Context;

import com.example.fannies.projectdocker.dao.DockerContainerEachStatCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerStatisticCollectionDao;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class DockerContainerEachStatManager {

    private static DockerContainerEachStatManager instance;
    private DockerContainerEachStatCollectionDao dao;

    public static DockerContainerEachStatManager getInstance() {
        if (instance == null)
            instance = new DockerContainerEachStatManager();
        return instance;
    }

    private Context mContext;

    private DockerContainerEachStatManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public DockerContainerEachStatCollectionDao getDao() {
        return dao;
    }

    public void setDao(DockerContainerEachStatCollectionDao dao) {
        this.dao = dao;
    }
}
