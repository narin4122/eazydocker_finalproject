package com.example.fannies.projectdocker.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.fannies.projectdocker.R;
import com.example.fannies.projectdocker.activity.HostActivity;
import com.example.fannies.projectdocker.activity.ViewProcessActivity;
import com.example.fannies.projectdocker.adapter.ContainerAdapter;
import com.example.fannies.projectdocker.adapter.HostListAdapter;
import com.example.fannies.projectdocker.dao.DockerContainerCollectionDao;
import com.example.fannies.projectdocker.dao.DockerContainerProcessCollectionDao;
import com.example.fannies.projectdocker.dao.HostItemCollectionDao;
import com.example.fannies.projectdocker.dao.SetThesholdCheckDao;
import com.example.fannies.projectdocker.manager.DockerContainerManager;
import com.example.fannies.projectdocker.manager.DockerContainerProcessManager;
import com.example.fannies.projectdocker.manager.HostListManager;
import com.example.fannies.projectdocker.manager.HttpManager;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by nuuneoi on 11/16/2014.
 */
@SuppressWarnings("unused")
public class HostContainerFragment extends Fragment {

    private int positionOfHost;
    private String hostUrl;
    private String hostPort;
    private String dockerName;
    private SwipeMenuCreator creator;
    private SwipeMenuListView listView;
    private ContainerAdapter listAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LayoutInflater inflater;

    private String[] CONTROL = {"START", "STOP", "PAUSE", "UNPAUSE", "RESTART", "SET THESHOLD"};

    public HostContainerFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static HostContainerFragment newInstance(int who) {
        HostContainerFragment fragment = new HostContainerFragment();
        Bundle args = new Bundle();
        args.putInt("who", who);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);
        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_host_container, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
        positionOfHost = getArguments().getInt("who");
        hostUrl = HostListManager.getInstance().getDao().getHost().get(positionOfHost).getHostUrl().toString();
        hostPort = HostListManager.getInstance().getDao().getHost().get(positionOfHost).getPort().toString();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        listView = (SwipeMenuListView) rootView.findViewById(R.id.container);
        listAdapter = new ContainerAdapter();
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                refreshWhenOptionOnSelectedSuccess(hostUrl, hostPort);
            }
        });
        listView.setOnItemClickListener(listViewItemClickListener);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                return;
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                swipeRefreshLayout.setEnabled(firstVisibleItem == 0);
            }
        });
        listAdapter.setListener(new ContainerAdapter.ContainerAdapterListener() {
            @Override
            public void onControlClick(final int position) {
                dockerName = DockerContainerManager.getInstance().getDao().getHost().get(position).getName().toString();
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Docker Control");
                builder.setItems(CONTROL, new DialogInterface.OnClickListener() {
                    Call<Void> callForControl;

                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                callForControl = HttpManager.getInstance().getServeice().startDockerContainer(hostUrl, hostPort, dockerName);
                                callForControl.enqueue(new Callback<Void>() {
                                    @Override
                                    public void onResponse(Call<Void> call, Response<Void> response) {
                                        showToast(CONTROL[0] + " success");
                                        refreshWhenOptionOnSelectedSuccess(hostUrl, hostPort);
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onFailure(Call<Void> call, Throwable t) {
                                        showToast(getContext().getResources().getString(R.string.onFailure));
                                    }
                                });
                                break;
                            case 1:
                                callForControl = HttpManager.getInstance().getServeice().stopDockerContainer(hostUrl, hostPort, dockerName);
                                callForControl.enqueue(new Callback<Void>() {
                                    @Override
                                    public void onResponse(Call<Void> call, Response<Void> response) {
                                        showToast(CONTROL[1] + " success");
                                        refreshWhenOptionOnSelectedSuccess(hostUrl, hostPort);
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onFailure(Call<Void> call, Throwable t) {
                                        showToast(getContext().getResources().getString(R.string.onFailure));
                                    }
                                });
                                break;
                            case 2:
                                callForControl = HttpManager.getInstance().getServeice().pauseDockerContainer(hostUrl, hostPort, dockerName);
                                callForControl.enqueue(new Callback<Void>() {
                                    @Override
                                    public void onResponse(Call<Void> call, Response<Void> response) {
                                        showToast(CONTROL[2] + " success");
                                        refreshWhenOptionOnSelectedSuccess(hostUrl, hostPort);
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onFailure(Call<Void> call, Throwable t) {
                                        showToast(getContext().getResources().getString(R.string.onFailure));
                                    }
                                });
                                break;
                            case 3:
                                callForControl = HttpManager.getInstance().getServeice().unPauseDockerContainer(hostUrl, hostPort, dockerName);
                                callForControl.enqueue(new Callback<Void>() {
                                    @Override
                                    public void onResponse(Call<Void> call, Response<Void> response) {
                                        showToast(CONTROL[3] + " success");
                                        refreshWhenOptionOnSelectedSuccess(hostUrl, hostPort);
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onFailure(Call<Void> call, Throwable t) {
                                        showToast(getContext().getResources().getString(R.string.onFailure));
                                    }
                                });
                                break;
                            case 4:
                                callForControl = HttpManager.getInstance().getServeice().restartDockerContainer(hostUrl, hostPort, dockerName);
                                callForControl.enqueue(new Callback<Void>() {
                                    @Override
                                    public void onResponse(Call<Void> call, Response<Void> response) {
                                        showToast(CONTROL[4] + " success");
                                        refreshWhenOptionOnSelectedSuccess(hostUrl, hostPort);
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onFailure(Call<Void> call, Throwable t) {
                                        showToast(getContext().getResources().getString(R.string.onFailure));
                                    }
                                });
                                break;
                            case 5:
                                dockerName = DockerContainerManager.getInstance().getDao().getHost().get(position).getName().toString();
                                inflater = LayoutInflater.from(getActivity());
                                final View thesholdDialog = inflater.inflate(R.layout.dialog_settheshold, null);
                                final TextView tvOldName = (TextView) thesholdDialog.findViewById(R.id.tvOldName);
                                final EditText edtCpuSet = (EditText) thesholdDialog.findViewById(R.id.edtCpuAlert);
                                final EditText edtMemSet = (EditText) thesholdDialog.findViewById(R.id.edtMemAlert);
                                tvOldName.setText(dockerName.toString());
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setView(thesholdDialog);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        if (edtCpuSet.getText().toString().isEmpty()) {
                                            edtCpuSet.setText("0");
                                        }
                                        if (edtMemSet.getText().toString().isEmpty()) {
                                            edtMemSet.setText("0");
                                        }
                                        Call<SetThesholdCheckDao> call = HttpManager.getInstance().getServeice().setTheshold(
                                                hostUrl, hostPort, dockerName
                                                , Float.parseFloat(edtCpuSet.getText().toString())
                                                , Float.parseFloat(edtMemSet.getText().toString())
                                        );
                                        call.enqueue(new Callback<SetThesholdCheckDao>() {
                                            @Override
                                            public void onResponse(Call<SetThesholdCheckDao> call, Response<SetThesholdCheckDao> response) {
                                                if (response.isSuccessful()) {
                                                    SetThesholdCheckDao dao = response.body();
                                                    if (dao.getCheck()) {
                                                        Toast.makeText(getActivity(), "Set theshold success!", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(getActivity(), "Update theshold success!", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<SetThesholdCheckDao> call, Throwable t) {
                                                Toast.makeText(getActivity(), getContext().getResources().getString(R.string.onFailure)
                                                        , Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }

                                });
                                builder.setNegativeButton("CANCLE", null);
                                builder.create();
                                builder.show();
                                break;
                        }
                    }
                });
                builder.setNegativeButton("CANCLE", null);
                builder.create();
                builder.show();
            }

            @Override
            public void onRenameClick(int position) {
                dockerName = DockerContainerManager.getInstance().getDao().getHost().get(position).getName().toString();
                inflater = LayoutInflater.from(getActivity());
                final View renameDialog = inflater.inflate(R.layout.dialog_rename, null);
                final TextView tvOldName = (TextView) renameDialog.findViewById(R.id.tvOldName);
                final TextView edtNewName = (EditText) renameDialog.findViewById(R.id.edtNewName);
                tvOldName.setText(dockerName.toString());
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(renameDialog);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    Call<Void> callForRename;

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!edtNewName.getText().toString().isEmpty()) {
                            callForRename = HttpManager.getInstance().getServeice().renameContainer(hostUrl, hostPort
                                    , dockerName, edtNewName.getText().toString());
                            callForRename.enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Call<Void> call, Response<Void> response) {
                                    showToast("Rename success");
                                    refreshWhenOptionOnSelectedSuccess(hostUrl, hostPort);
                                }

                                @Override
                                public void onFailure(Call<Void> call, Throwable t) {
                                    showToast(getContext().getResources().getString(R.string.onFailure));
                                }
                            });
                        } else {
                            dialog.dismiss();
                        }
                    }
                });
                builder.setNegativeButton("CANCLE", null);
                builder.create();
                builder.show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * Save Instance State Here
     **/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /**
     * Restore Instance State Here
     **/
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    public void showToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    public void refreshWhenOptionOnSelectedSuccess(String url, String port) {
        Call<DockerContainerCollectionDao> refresh = HttpManager.getInstance().getServeice().listDockerContainer(url, port);
        refresh.enqueue(new Callback<DockerContainerCollectionDao>() {
            @Override
            public void onResponse(Call<DockerContainerCollectionDao> call, Response<DockerContainerCollectionDao> response) {
                if (response.isSuccessful()) {
                    DockerContainerCollectionDao dao = response.body();
                    DockerContainerManager.getInstance().setDao(dao);
                    listAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<DockerContainerCollectionDao> call, Throwable t) {
                showToast("Refresh fail.");
                return;
            }
        });
    }

    final AdapterView.OnItemClickListener listViewItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            dockerName = DockerContainerManager.getInstance().getDao().getHost().get(position).getName().toString();
            Call<DockerContainerProcessCollectionDao> call = HttpManager.getInstance().getServeice().getProcess(hostUrl, hostPort, dockerName);
            call.enqueue(new Callback<DockerContainerProcessCollectionDao>() {
                @Override
                public void onResponse(Call<DockerContainerProcessCollectionDao> call, Response<DockerContainerProcessCollectionDao> response) {
                    DockerContainerProcessCollectionDao dao = response.body();
                    if (response.isSuccessful()) {
                        DockerContainerProcessManager.getInstance().setDao(dao);
                        startActivity(new Intent(getActivity(), ViewProcessActivity.class));
                    } else {
                        showToast("This container is exited state.");
                    }
                }

                @Override
                public void onFailure(Call<DockerContainerProcessCollectionDao> call, Throwable t) {
                    showToast(getContext().getResources().getString(R.string.onFailure));
                }
            });

        }
    };
}
