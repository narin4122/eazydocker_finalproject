package com.example.fannies.projectdocker.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fannies.projectdocker.R;
import com.example.fannies.projectdocker.activity.HostActivity;
import com.example.fannies.projectdocker.adapter.AlertAdapter;
import com.example.fannies.projectdocker.adapter.ContainerAdapter;
import com.example.fannies.projectdocker.dao.DockerContainerPeakCollectionDao;
import com.example.fannies.projectdocker.manager.DockerContainerManager;
import com.example.fannies.projectdocker.manager.DockerContainerPeakManager;
import com.example.fannies.projectdocker.manager.HostListManager;
import com.example.fannies.projectdocker.manager.HttpManager;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by nuuneoi on 11/16/2014.
 */
@SuppressWarnings("unused")
public class HostDashboardFragment extends Fragment {

    private int who;
    private DockerContainerManager manager = DockerContainerManager.getInstance();
    private int running;
    private int stop;
    private int pause;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvStart;
    private TextView tvStop;
    private TextView tvPause;
    private TextView tvAlert;
    private ListView listView;
    private AlertAdapter alertAdapter;

    public HostDashboardFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static HostDashboardFragment newInstance(int who) {
        HostDashboardFragment fragment = new HostDashboardFragment();
        Bundle args = new Bundle();
        args.putInt("who", who);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_host_dashboard, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        who = getArguments().getInt("who");
        calculateStateForShowInHostDashboardFragment();
    }

    public void calculateStateForShowInHostDashboardFragment() {
        running = 0;
        stop = 0;
        pause = 0;
        // Init Fragment level's variable(s) here
        for (int i = 0; i < manager.getDao().getHost().size(); i++) {
            String state = manager.getDao().getHost().get(i).getState();
            if (state.equals("running")) {
                running += 1;
                //Log.d("running", String.valueOf(running));
            } else if (state.equals("exited")) {
                stop += 1;
            } else if (state.equals("paused")) {
                pause += 1;
            }
        }
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById he

        listView = (ListView) rootView.findViewById(R.id.listView);

        View header = getLayoutInflater(savedInstanceState).inflate(R.layout.dashboard_header, null, false);
        listView.addHeaderView(header);
        alertAdapter = new AlertAdapter();
        listView.setAdapter(alertAdapter);
        alertAdapter.notifyDataSetChanged();
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                calculateStateForShowInHostDashboardFragment();
                swipeRefreshLayout.setRefreshing(false);
                tvStart.setText(running + "");
                tvStop.setText(stop + "");
                tvPause.setText(pause + "");
                tvAlert.setText(alertAdapter.getCount() + "");
                alertAdapter.notifyDataSetChanged();

            }
        });
        tvStart = (TextView) header.findViewById(R.id.sss);
        tvStart.setText(running + "");
        tvStop = (TextView) header.findViewById(R.id.stop);
        tvStop.setText(stop + "");
        tvPause = (TextView) header.findViewById(R.id.pause);
        tvPause.setText(pause + "");
        tvAlert = (TextView) header.findViewById(R.id.tvAlert);
        tvAlert.setText(alertAdapter.getCount() + "");


    }

    @Override
    public void onStart() {
        calculateStateForShowInHostDashboardFragment();
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }
}
